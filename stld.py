def cstolot(cs):
    lot=[]
    for i in cs.split(";"):
        t=tuple(i.split("="))
        lot.append(t)
    return lot
def lottocs(mylist):
    cs=""
    for k,v in mylist:
        cs += "{}={};".format(k,v)
    cs=cs.strip(';')
    return cs

def cstodict(cs):
    d={}
    for i in cs.split(";"):
        k,v=i.split("=")
        d[k]=v
    return d

def main():
    cs="a=b;c=d;e=f;g=h"
    lot=cstolot(cs)
    print(lot)
    mycs=lottocs(lot)
    print(mycs)
    d=cstodict(cs)
    print(d)
     
main()
